import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PointOfInterest } from './point-of-interest';

@Entity()
export class World {
  @PrimaryColumn()
  id: string;

  @Column({
    type: 'bigint',
    name: 'x',
    unique: true,
  })
  x: number;

  @Column({
    type: 'bigint',
    name: 'y',
    unique: true,
  })
  y: number;

  @OneToMany(() => PointOfInterest, (poi) => poi.world, {
    eager: true,
    cascade: ['insert'],
  })
  spacePointOfInterests?: PointOfInterest[];
}
