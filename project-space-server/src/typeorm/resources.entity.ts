import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PointOfInterest } from './point-of-interest';

@Entity()
export class Resource {
  @PrimaryGeneratedColumn({
    type: 'integer',
  })
  id: number;
  @Column({
    nullable: false,
    default: '',
  })
  name: string;
  @Column({
    nullable: false,
    default: '',
  })
  description: string;
  @Column({
    nullable: false,
    type: 'integer',
  })
  value: number;
  @Column({
    nullable: false,
    type: 'integer',
  })
  weight: number;
  @Column({
    nullable: false,
    type: 'boolean',
  })
  stackable: boolean;
  @Column({
    nullable: false,
    type: 'integer',
  })
  stackSize: number;

  /*   @ManyToMany(() => PointOfInterest, {
    eager: true,
    nullable: false,
  })
  @JoinTable()
  pointOfInterest: PointOfInterest; */
}
