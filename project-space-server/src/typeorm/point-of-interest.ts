import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  ManyToOne,
  JoinColumn,
  OneToMany,
  PrimaryColumn,
} from 'typeorm';
import { Resource } from './resources.entity';
import { World } from './world.entity';

@Entity()
export class PointOfInterest {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({
    nullable: false,
    default: '',
  })
  name: string;

  @Column({
    nullable: false,
    type: 'integer',
  })
  type: number;

  @Column({
    nullable: false,
    default: '',
  })
  description: string;
  /* 
  @ManyToMany(() => Resource, {
    eager: true,
    nullable: true,
  })
  resource: Resource[];
 */
  @ManyToOne(() => World, (world) => world.spacePointOfInterests)
  world: World;
}
