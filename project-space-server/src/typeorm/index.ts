import { PointOfInterest } from './point-of-interest';
import { Resource } from './resources.entity';
import { Ships } from './ships.entity';
import { User } from './user.entity';
import { World } from './world.entity';
const entities = [User, World, PointOfInterest, Resource, Ships];

export { User, World, PointOfInterest, Resource, Ships };
export default entities;
