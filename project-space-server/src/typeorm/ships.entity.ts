import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';

export interface ShipProperties {
  id: number;
  name: string;
  type: number;
  ownerId: number;
  hitpoints: number;
  xVelocity: number;
  yVelocity: number;
}

@Entity()
export class Ships {
  @PrimaryGeneratedColumn({
    type: 'integer',
  })
  id?: number;

  @Column({
    nullable: false,
    default: '',
  })
  name: string;

  @Column({
    nullable: false,
    type: 'integer',
  })
  type: number;

  @ManyToOne(() => User, {
    eager: true,
    nullable: false,
  })
  owner: User;

  @Column({
    nullable: false,
    type: 'integer',
  })
  hitpoints: number;

  @Column({
    nullable: false,
    type: 'integer',
  })
  xVelocity: number;

  @Column({
    nullable: false,
    type: 'integer',
  })
  yVelocity: number;

  @Column({
    nullable: false,
    type: 'integer',
  })
  xPosition: number;

  @Column({
    nullable: false,
    type: 'integer',
  })
  yPosition: number;

  @Column('hstore')
  items: Record<string, string>;
}
