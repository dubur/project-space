import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Ships } from './ships.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  id: number;

  @Column({
    nullable: false,
    default: '',
    unique: true,
  })
  username: string;

  @OneToMany(() => Ships, (ship) => ship.owner)
  ships: Ships[];
}
