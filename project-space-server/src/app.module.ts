import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './endpoints/auth/auth.module';
import { UsersModule } from './endpoints/users/users.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import entities from './typeorm';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard } from './endpoints/auth/auth.guard';
import { WorldModule } from './endpoints/world/world.module';
import { GameLoopService } from './game-logic/game-loop/game-loop.service';
import { GameLogicModule } from './game-logic/game-logic.module';
import { EndpointsModule } from './endpoints/endpoints.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        autoLoadEntities: true,
        type: 'postgres',
        host: configService.get('DB_HOST'),
        port: +configService.get<number>('DB_PORT'),
        username: configService.get('DB_USERNAME'),
        password: configService.get('DB_PASSWORD'),
        database: configService.get('DB_NAME'),
        entities: entities,
        synchronize: true,
      }),
      inject: [ConfigService],
    }),
    GameLogicModule,
    EndpointsModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    GameLoopService,
  ],
})
export class AppModule {}
