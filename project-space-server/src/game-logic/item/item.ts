export interface Item {
  id: number;
  name: string;
  description: string;
  value: number;
  fuelValue: number;
  weight: number;
  resource: boolean;
  stackable: boolean;
  stackSize: number;
  equipable: boolean;
  equipSlot: string;
}
