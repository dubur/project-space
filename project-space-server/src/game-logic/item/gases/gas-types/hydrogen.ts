import { Gas } from '../gas';

export class Hydrogen extends Gas {
  constructor() {
    super({
      id: 0,
      name: 'Hydrogen',
      description:
        'A gas that is highly flammable and explosive. It is the most abundant element in the universe.',
      value: 1,
      fuelValue: 1,
      weight: 1,
      stackable: true,
      stackSize: 10000,
    });
  }
}
