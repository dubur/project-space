import { Item } from '../item';

export interface GasProperties {
  id: number;
  name: string;
  description: string;
  value: number;
  fuelValue: number;
  weight: number;
  stackable: boolean;
  stackSize: number;
}

export class Gas implements Item {
  id: number;
  name: string;
  description: string;
  value: number;
  fuelValue: number;
  weight: number;
  resource: boolean;
  stackable: boolean;
  stackSize: number;
  equipable: boolean;
  equipSlot: string;

  constructor(gasProperties: GasProperties) {
    this.id = gasProperties.id;
    this.name = gasProperties.name;
    this.description = gasProperties.description;
    this.value = gasProperties.value;
    this.fuelValue = gasProperties.fuelValue;
    this.weight = gasProperties.weight;
    this.resource = true;
    this.stackable = gasProperties.stackable;
    this.stackSize = gasProperties.stackSize;
    this.equipable = false;
    this.equipSlot = null;
  }
}
