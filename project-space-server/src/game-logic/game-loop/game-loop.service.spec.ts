import { Test, TestingModule } from '@nestjs/testing';
import { GameLoopService } from './game-loop.service';

describe('GameLoopService', () => {
  let service: GameLoopService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GameLoopService],
    }).compile();

    service = module.get<GameLoopService>(GameLoopService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
