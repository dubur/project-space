import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PointOfInterest, World } from '../../typeorm';
import { PointOfInterestService } from './point-of-interest.service';

@Module({
  imports: [TypeOrmModule.forFeature([PointOfInterest])],
  providers: [PointOfInterestService],
  exports: [PointOfInterestService],
  controllers: [],
})
export class PointOfInterestModule {}
