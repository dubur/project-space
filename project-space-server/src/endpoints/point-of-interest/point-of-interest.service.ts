import { Injectable } from '@nestjs/common';
import { PointOfInterest } from '../../typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { World } from '../../typeorm';

@Injectable()
export class PointOfInterestService {
  constructor(
    @InjectRepository(PointOfInterest)
    private readonly pointOfInterest: Repository<PointOfInterest>,
  ) {}
  // A function that generates a random number of points of interest for a planet
  pointOfInterestGenerator(world: World) {
    const numberOfPointsOfInterest = Math.ceil(Math.random() * 10);
    const pointsOfInterest = [];

    for (let i = 0; i < numberOfPointsOfInterest; i++) {
      const pointOfInterest: PointOfInterest = {
        name: 'planet1',
        type: 1,
        description: 'planet2',
        world,
      };
      pointsOfInterest.push(pointOfInterest);
    }

    return pointsOfInterest;
  }
}
