import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { WorldModule } from './world/world.module';
import { ShipsModule } from './ships/ships.module';
import { PointOfInterestModule } from './point-of-interest/point-of-interest.module';

@Module({
  imports: [
    AuthModule,
    ShipsModule,
    UsersModule,
    WorldModule,
    PointOfInterestModule,
  ],
  providers: [],
})
export class EndpointsModule {}
