export interface User {
  userId: number;
  username: string;
  password: string;
  email: string;
  createdOn: number;
  lastLogin: number;
}
