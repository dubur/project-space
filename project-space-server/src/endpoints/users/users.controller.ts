import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UsePipes,
  ConflictException,
} from '@nestjs/common';
import { CreateUserDto } from './users.dtos';
import { UsersService } from './users.service';
import { DEFAULT_VALIDATION_PIPE, Public } from '../../decorators/public';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Get()
  getUsers() {
    return this.userService.getUsers();
  }

  @Get('username/:username')
  findUsersById(@Param('username', DEFAULT_VALIDATION_PIPE) username: string) {
    return this.userService.findUsersById(username);
  }

  @Post('create')
  @Public()
  @UsePipes(DEFAULT_VALIDATION_PIPE)
  async createUsers(@Body() createUserDto: CreateUserDto) {
    return this.userService.createUser(createUserDto).catch((err) => {
      if (err.code === '23505') {
        throw new ConflictException('user already exists');
      }
    });
  }
}
