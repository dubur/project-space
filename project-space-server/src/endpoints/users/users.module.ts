import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../../typeorm';
import { UsersController } from './users.controller';
import { Ships } from '../../typeorm/ships.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Ships])],
  providers: [UsersService],
  controllers: [UsersController],
  exports: [UsersService],
})
export class UsersModule {}
