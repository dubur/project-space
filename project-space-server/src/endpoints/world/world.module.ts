import { Module } from '@nestjs/common';
import { WorldController } from './world.controller';
import { WorldService } from './world.service';
import { World } from '../../typeorm';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FeatureGenerationService } from './generation/feature-generation';
import { HttpModule } from '@nestjs/axios';
import { PointOfInterest } from '../../typeorm/point-of-interest';
import { Resource } from '../../typeorm/resources.entity';
import { PointOfInterestModule } from '../point-of-interest/point-of-interest.module';

@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forFeature([World, Resource]),
    PointOfInterestModule,
  ],
  providers: [WorldService],
  controllers: [WorldController],
})
export class WorldModule {}
