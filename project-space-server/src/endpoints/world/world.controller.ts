import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Coordinates, WorldService } from './world.service';

@Controller('world')
export class WorldController {
  constructor(private readonly worldService: WorldService) {}

  @Post('scan')
  @UsePipes(ValidationPipe)
  async scanCoordinates(@Body() coordinates: Coordinates) {
    return await this.worldService.scanCoordinates(coordinates);
  }

  @Get('scan/:x/:y')
  @UsePipes(ValidationPipe)
  async getCoordinateFeatures(@Param('x') x, @Param('y') y) {
    return await this.worldService.getCoordinateFeatures({ x, y });
  }
}
