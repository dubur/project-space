import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { World } from '../../typeorm';
import { FeatureGenerationService } from './generation/feature-generation';
import { SolarSystemFeatures } from './feature/feature';
import { PointOfInterestService } from '../point-of-interest/point-of-interest.service';

export interface Coordinates {
  x: number;
  y: number;
}

export interface PlanetProperties {
  name: string;
  description: string;
  resources: string[];
  other: string;
}

@Injectable()
export class WorldService {
  constructor(
    @InjectRepository(World)
    private readonly worldRepository: Repository<World>,
    private readonly pointOfInterestService: PointOfInterestService,
  ) {}

  async scanCoordinates(coordinates: Coordinates) {
    let typeId = Math.ceil(Math.random() * 11);
    typeId = 1;

    /*     const featureType = SolarSystemFeatures[typeId];

    const featureDescription = await this.featureService.createCompletion(
      `Imagine a new planet and name it. Then describe the planet. Then create a list of chemical formulas that exist on the planet in a csv list.
`,
    ); */

    /* example chatgpt prompt
    `
    Imagine a new planet and name it. Then describe the planet. Then create an array of chemical formulas strings that could exist on the planet

Format your response as follows:
Planet_name: {{planetName}}

Planet_description: {{planetDescription}}

Planet_chemical_formulas: {{planetResources}}

Other: $other`;
 */

    const exampleResponse = `Planet_name: Aetheria

Planet_description: Aetheria is a breathtaking celestial body located in a distant star system known as Luminae. It is a realm of enchantment and wonder, characterized by its ethereal and luminescent landscapes. Aetheria is bathed in a soft, iridescent glow that emanates from the myriad of glowing crystals that cover its surface. The planet is adorned with vast floating islands suspended in the atmosphere, each one a verdant paradise adorned with exotic flora and cascading waterfalls. Aetheria experiences gentle, shimmering rains that shower the land, nurturing the vibrant plant life and creating delicate, colorful mists that envelop the floating islands.

Planet_chemical_formulas:

Luminite (Lu): Al2O3
Ethereon (En): C6H10O5
Crystallium (Cr): SiO2
Aetherium (Am): C10H14N2O3
Celestite (Ce): SrSO4
Prismatic (Pm): C5H9NO4
Lumina (La): C8H12O2
Radiantite (Rt): CaSO4·2H2O
Glitterstone (Gs): KAlSi3O8
Enchantium (Et): Fe2O3·nH2O
Other:
Aetheria is home to an extraordinary race of beings known as the Luminalis. These ethereal creatures possess a deep connection to the planet's energy and harness the power of light and crystal magic. The Luminalis are known for their intricate and delicate artistry, crafting elaborate sculptures and creating mesmerizing light displays that illuminate the night sky. They live in harmony with the natural wonders of Aetheria, cherishing the balance between the ethereal and the earthly. Aetheria is a realm of serenity and spiritual awakening, inviting visitors to experience its transcendent beauty and partake in the Luminalis' wisdom and creativity.`;

    /*    const planetProperties = this.handleChatGPTResponse(exampleResponse);

    const currentPlanet = await this.worldRepository.findOne({
      where: { x: coordinates.x, y: coordinates.y },
    });

    let spacePointOfInterest = currentPlanet?.spacePointOfInterest
      ? currentPlanet.spacePointOfInterest
      : [];
    spacePointOfInterest = [...spacePointOfInterest, { ...planetProperties }];

    const newWorld: World = {
      ...coordinates,
      spacePointOfInterest,
    }; */
    let world: World = {
      id: `${coordinates.x}:${coordinates.y}`,
      ...coordinates,
    };
    world = {
      ...world,
      spacePointOfInterests:
        this.pointOfInterestService.pointOfInterestGenerator(world),
    };

    console.log(world);
    return await this.worldRepository.save(world);
  }

  handleChatGPTResponse(response: string): PlanetProperties {
    const handledResponse = response
      .split('\n')
      .filter((line: string) => line !== '');

    const title = handledResponse[0].split(':')[1];
    const description = handledResponse[1].split(':')[1];

    const resources: string[] = [];

    for (let i = 3; i < handledResponse.length; i++) {
      if (handledResponse[i].includes('Other:')) {
        break;
      }
      resources.push(handledResponse[i].split(':')[0]);
    }

    return {
      name: title,
      description,
      resources,
      other: '',
    };
  }

  getCoordinateFeatures(coordinates: Coordinates) {
    console.log(coordinates);
    return this.worldRepository.find({
      relations: {
        spacePointOfInterests: true,
      },
    });
  }
}
