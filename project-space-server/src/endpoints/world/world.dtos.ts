import { IsInt, IsJSON } from 'class-validator';
import { IsEmail, IsNotEmpty, MinLength, isNotEmpty } from 'class-validator';
import { PrimaryColumn } from 'typeorm';

export class SpacePointOfInterestDto {
  @IsNotEmpty()
  @IsInt()
  x: number;

  @IsNotEmpty()
  @IsInt()
  y: number;
}
