import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { Configuration, OpenAIApi } from 'openai';
import { Observable, firstValueFrom, map } from 'rxjs';

@Injectable()
export class FeatureGenerationService {
  configuration: Configuration;
  openai: OpenAIApi;

  constructor(private readonly httpService: HttpService) {}

  async createCompletion(prompt: string): Promise<any> {
    return firstValueFrom(
      this.httpService
        .post(
          'http://127.0.0.1:4891/v1/chat/completions',
          {
            model: 'v3-13b-hermes-q5_1',
            messages: [{ role: 'user', content: prompt }],
            temperature: 0.9,
            max_tokens: 8000,
            top_p: 0.8,
            n: 1,
            echo: true,
            stream: true,
          },
          {
            headers: {
              'Content-Type': 'application/json',
              Accept: '*/*',
              'Accept-Encoding': 'gzip, deflate, br',
              'Access-Control-Allow-Origin': '*',
              Connection: 'keep-alive',
            },
          },
        )
        .pipe(
          map((res) => {
            console.log('res', res.data.choices[0].message.content);
            return res.data.choices[0].message.content;
          }),
        ),
    );

    /*     return await this.openai.createCompletion({
      model: 'gpt4all-l13b-snoozy',
      prompt,
      max_tokens: 8000,
      top_p: 0.95,
      n: 1,
      echo: true,
      stream: false,
    }); */
  }
}
