// Code that generates some different solarsystem features to add to the game (e.g. planets, asteroids, black holes, etc.) and returns them as an object

export enum SolarSystemFeatures {
  planet = 1,
  asteroid = 2,
  blackHole = 3,
  wormHole = 4,
  star = 5,
  nebula = 6,
  comet = 7,
  moon = 8,
  spaceStation = 9,
  spaceShip = 10,
  spaceDebris = 11,
}

export interface FeatureProperties {
  name: string;
  type: SolarSystemFeatures;
  description: string;
  mass: number;
}
