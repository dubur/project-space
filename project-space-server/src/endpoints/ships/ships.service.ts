import { Injectable } from '@nestjs/common';
import { CreateShipDto, SetVelocityShipDto, UpdateShipDto } from './ships.dtos';
import { Ships } from '../../typeorm/ships.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../../typeorm';

@Injectable()
export class ShipsService {
  constructor(
    @InjectRepository(Ships)
    private readonly shipsRepository: Repository<Ships>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async getShips(userId: number) {
    const user = await this.userRepository.findOne({
      where: { id: userId },
      relations: ['ships'],
    });

    return user.ships;
  }

  async createShip(createShipDto: CreateShipDto, userId: number) {
    const user = await this.userRepository.findOne({
      where: { id: userId },
    });

    const shipProperties: Ships = {
      ...createShipDto,
      hitpoints: 100,
      xVelocity: 0,
      yVelocity: 0,
      xPosition: 0,
      yPosition: 0,
      owner: user,
      items: { hydrogen: '1000' },
    };

    return await this.shipsRepository.save(shipProperties);
  }

  async updateShip(
    userId: number,
    newShipProperties: UpdateShipDto | SetVelocityShipDto,
  ) {
    let ship = await this.getShipById(userId, newShipProperties.id);

    ship = { ...ship, ...newShipProperties };

    return this.shipsRepository.update(newShipProperties.id, {
      ...ship,
    });
  }

  async getShipById(userId: number, shipId: number) {
    return await this.userRepository
      .createQueryBuilder('user')
      .where('user.id = :id', { id: userId })
      .leftJoinAndSelect('user.ships', 'ship')
      .where('ship.id = :id', { id: shipId })
      .getOneOrFail()
      .catch((err) => {
        console.log(err);
        throw err;
      })
      .then((user) => {
        return user.ships[0];
      });
  }
}
