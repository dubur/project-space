import { Optional } from '@nestjs/common';
import { IsNotEmpty, IsOptional, MinLength } from 'class-validator';

export class CreateShipDto {
  @IsNotEmpty()
  @MinLength(3)
  name: string;

  @IsNotEmpty()
  type: number;
}

export class UpdateShipDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  @IsOptional()
  @MinLength(3)
  name: string;
}

export class SetVelocityShipDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  @IsOptional()
  xVelocity: number;
  @IsNotEmpty()
  @IsOptional()
  yVelocity: number;
}
