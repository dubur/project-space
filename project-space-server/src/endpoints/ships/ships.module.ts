import { Module } from '@nestjs/common';
import { ShipsController } from './ships.controller';
import { ShipsService } from './ships.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Ships } from '../../typeorm/ships.entity';
import { User } from '../../typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Ships, User])],
  providers: [ShipsService],
  controllers: [ShipsController],
})
export class ShipsModule {}
