import {
  Body,
  Controller,
  Get,
  HttpException,
  Patch,
  Post,
  Req,
  UsePipes,
} from '@nestjs/common';
import { CreateShipDto, SetVelocityShipDto, UpdateShipDto } from './ships.dtos';
import { ShipsService } from './ships.service';
import { Request } from 'express';
import { EntityNotFoundError } from 'typeorm';
import { DEFAULT_VALIDATION_PIPE } from '../../decorators/public';

@Controller('ships')
export class ShipsController {
  constructor(private readonly shipService: ShipsService) {}

  @Patch('update')
  @UsePipes(DEFAULT_VALIDATION_PIPE)
  async updateShip(
    @Req() request: Request & { user: { id: string } },
    @Body() shipProperties: UpdateShipDto,
  ) {
    return await this.shipService
      .updateShip(+request.user.id, shipProperties)
      .catch((err) => {
        if (err instanceof EntityNotFoundError) {
          return new HttpException('Ship not found', 404);
        }
      });
  }

  @Get()
  async getShips(@Req() request: Request & { user: { id: string } }) {
    return await this.shipService.getShips(+request.user.id);
  }

  @Post('create')
  @UsePipes(DEFAULT_VALIDATION_PIPE)
  async createShip(
    @Req() request: Request & { user: { id: string } },
    @Body() CreateShipDto: CreateShipDto,
  ) {
    const userId = request.user.id;
    return await this.shipService.createShip(CreateShipDto, +userId);
  }

  @Patch('velocity')
  @UsePipes(DEFAULT_VALIDATION_PIPE)
  async setVelocity(
    @Req() request: Request & { user: { id: string } },
    @Body() setVelocityShipDto: SetVelocityShipDto,
  ) {
    const userId = request.user.id;
    return await this.shipService.updateShip(+userId, setVelocityShipDto);
  }
}
