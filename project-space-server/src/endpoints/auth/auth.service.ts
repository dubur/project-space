import { Injectable, NotFoundException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async signIn(username: string): Promise<{ accessToken: string } | Error> {
    const user = await this.usersService.findUsersById(username);

    if (!user) {
      return new NotFoundException('user not found');
    }
    // TODO add passwords

    // TODO: Generate a JWT and return it here
    // instead of the user object
    const payload = { id: user.id, username: user.username };
    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }
}
