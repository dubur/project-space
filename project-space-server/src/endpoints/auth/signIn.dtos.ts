import { IsNotEmpty, MinLength } from 'class-validator';
export class SignInDto {
  @IsNotEmpty()
  @MinLength(3)
  username: string;

  /*   @IsNotEmpty()
  @MinLength(8)
  password: string;

  @IsNotEmpty()
  @IsEmail()
  email: string; */
}
