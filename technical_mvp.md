# Task list (Essentially Jira)

- [ ] ${feature}
    - ${description}
    - <r>Not done</r> / <g>Done<g>
    - ${date}

- [ ] Setup database in repository
    - I believe we will not store a game state directly but rather have it be stored in a database and every action/inaction will be a call to the database. This will allow us to have an easy way of having a single source of truth.
    - The current idea is to use postgres. We will need the following tables
      - World map
        - Coordinate features
          - Stars 
          - Planets
            - Inhabited
            - Uninhabited
          - Asteroids
          - Space stations
          - Players
      - Player information
        - Ships
        - Inventory
        - Location
- [ ] Setup basic server
















<style>
r { color: Red }
o { color: Orange }
g { color: Green }
</style>
