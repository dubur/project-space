export const DB_SETTINGS_SETUP = {
  user: "postgres",
  host: "localhost",
  port: 5432,
};

export const DB_SETTINGS = { ...DB_SETTINGS_SETUP, database: "space" };
