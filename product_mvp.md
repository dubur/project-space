
# MVP Feature list:

**Infinite world (x,y grid) with players being able to explore any given coordinate**
  * Activities within the grid can be given decimal locations (e.g planet at 1.1, 2)
  * Generated when explored and stored in a DB (Ignore the scability issues here)
  * Some way to 'scan' coordinates
  * Coordinate features
    * Stars
    * inhabited planets
      * Contains e.g. markets
    * other planets
      * Contains e.g. resources
  * 2? FPS 

**Two basic generated activities** 
  * Mining some type of ore
  * Basic combat (Defeat space pirates)

**Ships** (SHIP SHOULD NOT BE TIED TO THE PLAYER BUT RATHER IT'S OWN ENTITY THE PLAYER CAN SEND)
  * At least two ship types with different module slots
  * Should have at least 5 different module types
    * Engines
    * Weapons
    * Armour
    * Misc (exploration scanners etc)
    * Resource gen (e.g. mining lasers)

**Crafting**
  * Two different ships and required resources
  * Modules
    * Look at building a framework for creating items (Think poe affixes)


**Character progression**
  * At least two nodes to unlock via some type of xp with a quantifiable bonus
    * One combaty
    * One non-combaty


**Some type of currency (money / energy ???)**
**Market**
  * Create a simulated PLANET market with at least 1 resource
    * Sell
    * Buy