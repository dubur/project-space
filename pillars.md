# Space Pillars  (THE VISION)

**Automation**
  * **NO SUPPLIED UI** (There is no plan to add a given ui but give the tools for interacting with the world instead via api calls)
  * **Programmable**
    * Host API to allow someone to 'play' the game via api calls
      * Build their own scripts for all parts of the game (Build default examples if one can ever be bothered but not important)

**Dynamic/Living (Worlds / Players / Items)**
  * Exploration
    * Infinite world to allow players to either create their own part of universe or play more central for indirect interactions
    * Can travel anywhere but limited to fuel so would have to find refuelling sources
  * Async economy (For balancing with low player count (or even 1 player))
    * Money
    * No direct trade but player actions changes e.g. market prices for other people
      * Simulate an economy and simulate ever changing market conditions (easy right, just do it)
  * New resources or resource quality
    * Procedual generation of resources or quality or smthing
    * Corruption in poe for modifiers when crafting with various resources, change outcome likelihood or something similar
  * Impact of players on the world 
    * (e.g more players mining in a system = less resources from that system)
    * More players crafting in a system = more crafting multipler from local expertise or smthing
    * incentives transporting of resources etc etc etc
  * Difficulty scaling of world
    * Different systems have some kind of danger rating 
   

**Growing complexity**
  * Crafting (based on character stats?)
    * Ships
      * tiers/sizes
        * eve like corvette -> titans (Titans take a long time to fly)
        * Aim to introduce more as players reach it to create endless progression and create sufficiently risk/rewarding activities
      * Varying module slots???
      * Expendable (Essentially high tier ammunition)
    * Modules
      * Varying module progression
        * Combat
          * weapons
          * armour etc etc
        * Non-combat
          * mining etc etc
  * Fleet management
    * E.g. x combat ships for 
  * Character (represents permanent progression)
      * xp???
          * Eve like skill system?
          * Albion like skill system? 
          * Osrs ?
      * bonuses
          * who the fuck knows but this is the main source of permanent progression so you are never fully reset to 0
      
**Activity choice**
  * I'd love to have players be able to supply their own scripts for 'encounters', e.g. you can write a script defining your ships combat logic. (e.g. target opponents engines above all else) but I don't see this happening
  * Resource generation
    * Mining
    * Energy
    * Food
  * Combat
    * Piracy (Either conduct or prevent) (System have a soft capped minimum safety for new player systems etc)
      * Makes a system safer/more dangerous to promote various activities, e.g. populated systems are safe and become crafting hubs but unexplored ones promote efficient resource generation but requires more safeguarding
    * Other factions/species ?
  * Crafting
    * Simple -> Complex crafting chains

**Endless**
  * :)