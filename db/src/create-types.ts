import { Client } from "pg";
import { DB_SETTINGS } from "../../global_config/db-settings";
const client = new Client(DB_SETTINGS);
client.connect().then(async () => {
  await createType(`CREATE TYPE worldFeatures AS (x INT, y INT, type CHAR[]);`);
  client.end();
});

async function createType(typeSQL: string) {
  console.log("here");
  return await client.query(typeSQL);
}
