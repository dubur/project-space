import { Client } from "pg";
import { DB_SETTINGS } from "../../global_config/db-settings";

const client = new Client(DB_SETTINGS);
client.connect().then(() => {
  let userTableColsString = `(
     userId serial PRIMARY KEY,
     username VARCHAR ( 50 ) UNIQUE NOT NULL,
     password VARCHAR ( 50 ) NOT NULL,
     email VARCHAR ( 255 ) UNIQUE NOT NULL,
     createdOn TIMESTAMP NOT NULL,
     lastLogin TIMESTAMP);`;
  createTable({ name: "users", colsString: userTableColsString });

  /* let worldTableColsString = `(
       x INT NOT NULL,
       y INT NOT NULL,
       features // 
       );`; */
  async function createTable(tableProp: { name: string; colsString: string }) {
    await client.query(
      `CREATE TABLE IF NOT EXISTS ${tableProp.name}${tableProp.colsString}`
    );
    await client.end();
    console.log("Created database:", tableProp.name);
  }
});
