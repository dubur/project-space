import { Client } from "pg";
import { DB_SETTINGS_SETUP } from "../../global_config/db-settings";
const client = new Client(DB_SETTINGS_SETUP);

createDatabase({ dropCurrentDB: true, dbname: "space" });

async function createDatabase(values: {
  dbname: string;
  dropCurrentDB: boolean;
}) {
  await client.connect();

  if (values.dropCurrentDB) {
    await client.query(`DROP DATABASE IF EXISTS ${values.dbname};`);
    await client.query(`CREATE DATABASE ${values.dbname};`);
  }
  await client.end();
  console.log("Created database:", values.dbname);
}
